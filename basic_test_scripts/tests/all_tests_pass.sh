#!/bin/bash

set -e

source ./lib.sh

# Tests

binary_exists /bin/ls

binary_exists /bin/cat

binary_exists /bin/ln

check_fails
