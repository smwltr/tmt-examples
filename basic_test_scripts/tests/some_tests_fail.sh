#!/bin/bash

set -e

source ./lib.sh

# Tests

binary_exists /bin/ln

binary_exists /bin/cat

binary_exists /bin/fake

binary_exists /bin/ls

check_fails
